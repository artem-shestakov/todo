# ToDo App
## Preparation
Create `.env` file:
``` bash
DB_NAME=todo
DB_USER=user
DB_PASSWORD=password
```
## Run
1. Run docker conteiners bu docker-compose
```bash
$ docker-compose up -d
```
2. Prepare database by first migrate
``` bash
$ docker-compose exec app migrate -path /migrations -database 'postgres://<user>:<password>@db:5432/todo?sslmode=disable' up
```