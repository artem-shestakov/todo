define({ "api": [
  {
    "type": "post",
    "url": "/auth/sign-in",
    "title": "Get JWT token",
    "group": "Auth",
    "name": "SignIn",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>User's email (login)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>User login password</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Example:",
          "content": "\t{\n   \t\"email\": \"artem.s.shestakov@yandex.ru\",\n  \t\"password\": \"userPassword\"\n\t}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>JWT token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "200:",
          "content": "\t{\n  \t\"token\": \"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MjI1NzI4MTQsImlhdCI6MTYyMjUyOTYxNCwidXNlcl9pZCI6Nn0.8Pg3iFwsC1hs_y3VHPsllgh_szeZkxVUNZ4a5YunTy4\"\n\t}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "4xx": [
          {
            "group": "4xx",
            "type": "String",
            "optional": false,
            "field": "error",
            "description": "<p>Error description</p>"
          }
        ],
        "5xx": [
          {
            "group": "5xx",
            "type": "String",
            "optional": false,
            "field": "error",
            "description": "<p>Error description</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "400:",
          "content": "\t{\n   \t\"error\": \"400 - Unmarshall error: invalid character '\\\\n' in string literal\"\n\t}",
          "type": "json"
        },
        {
          "title": "500:",
          "content": "\t{\n   \t\"error\": \"500 - Sign in error: sql: no rows in result set\"\n\t}",
          "type": "json"
        }
      ]
    },
    "filename": "./internal/app/handler/auth.go",
    "groupTitle": "Auth"
  },
  {
    "type": "post",
    "url": "/auth/sigh-up",
    "title": "Create new user",
    "name": "SignUp",
    "group": "Auth",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "first_name",
            "description": "<p>User's first name</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "last_name",
            "description": "<p>User's last name</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>User's email (login)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>User login password</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Example:",
          "content": "\t{\n   \t\"first_name\": \"Artem\",\n   \t\"last_name\": \"Shestakov\",\n   \t\"email\": \"artem.s.shestakov@yandex.ru\",\n  \t\"password\": \"userPassword\"\n\t}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "201": [
          {
            "group": "201",
            "type": "String",
            "optional": false,
            "field": "user_id",
            "description": "<p>Created user's ID</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "201:",
          "content": "\t{\n  \t\"user_id\": \"1\"\n\t}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "4xx": [
          {
            "group": "4xx",
            "type": "String",
            "optional": false,
            "field": "error",
            "description": "<p>Error description</p>"
          }
        ],
        "5xx": [
          {
            "group": "5xx",
            "type": "String",
            "optional": false,
            "field": "error",
            "description": "<p>Error description</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "400",
          "content": "\t{\n   \t\"error\": \"400 - Error with unmarshall JSON: invalid character '\\\\n' in string literal\"\n\t}",
          "type": "json"
        },
        {
          "title": "500",
          "content": "\t{\n   \t\"error\": \"500 - Create user error: pq: duplicate key value violates unique constraint \\\"users_email_key\\\"\"\n\t}",
          "type": "json"
        }
      ]
    },
    "filename": "./internal/app/handler/auth.go",
    "groupTitle": "Auth"
  },
  {
    "type": "post",
    "url": "api/v1/board",
    "title": "Create new board",
    "name": "CreateBoard",
    "group": "Board",
    "version": "1.0.0",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Bearer authorization token.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n    \"Authorization\": \"Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MjI2Njg0NzcsImlhdCI6MTYyMjYyNTI3NywidXNlcl9pZCI6MTB9.0aOR7SJHE9ycZa0ihPR6Q7rICCjKPauUbEiUJw16aPo\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "titte",
            "description": "<p>Board title</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "description",
            "description": "<p>Board description</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Example:",
          "content": "\t{\n   \t\"title\": \"Work\",\n   \t\"description\": \"Board for work tasks\",\n\t}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "201": [
          {
            "group": "201",
            "type": "String",
            "optional": false,
            "field": "user_id",
            "description": "<p>Created board's ID</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "201:",
          "content": "\t{\n  \t\"board_id\": \"1\"\n\t}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "4xx": [
          {
            "group": "4xx",
            "type": "String",
            "optional": false,
            "field": "error",
            "description": "<p>Error description</p>"
          }
        ],
        "5xx": [
          {
            "group": "5xx",
            "type": "String",
            "optional": false,
            "field": "error",
            "description": "<p>Error description</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "400",
          "content": "\t{\n   \t\"error\": \"400 - JSON reading error invalid character '\\\\n' in string literal\"\n\t}",
          "type": "json"
        },
        {
          "title": "401",
          "content": "\t{\n   \t\"error\": \"401 - Empty Authorization header\"\n\t}",
          "type": "json"
        },
        {
          "title": "500",
          "content": "\t{\n   \t\"error\": \"500 - dial tcp database.local:5432: connect: connection refused\"\n\t}",
          "type": "json"
        },
        {
          "title": "500",
          "content": "\t{\n   \t\"error\": \"500 - Can't get user ID from request\"\n\t}",
          "type": "json"
        }
      ]
    },
    "filename": "./internal/app/handler/board.go",
    "groupTitle": "Board"
  },
  {
    "type": "delete",
    "url": "api/v1/board/:id",
    "title": "Delete user's board",
    "name": "DeleteBoard",
    "group": "Board",
    "version": "1.0.0",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Bearer authorization token.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n    \"Authorization\": \"Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MjI2Njg0NzcsImlhdCI6MTYyMjYyNTI3NywidXNlcl9pZCI6MTB9.0aOR7SJHE9ycZa0ihPR6Q7rICCjKPauUbEiUJw16aPo\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>User's board ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "boards",
            "description": "<p>List of user's boards</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "200:",
          "content": "\t{\n      \"deleted_board_id\": 1\n  }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "4xx": [
          {
            "group": "4xx",
            "type": "String",
            "optional": false,
            "field": "error",
            "description": "<p>Error description</p>"
          }
        ],
        "5xx": [
          {
            "group": "5xx",
            "type": "String",
            "optional": false,
            "field": "error",
            "description": "<p>Error description</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "400",
          "content": "\t{\n   \t\"error\": \"400 - Invalid ID param\"\n\t}",
          "type": "json"
        },
        {
          "title": "401",
          "content": "\t{\n   \t\"error\": \"401 - Invalid Authorization header\"\n\t}",
          "type": "json"
        },
        {
          "title": "404",
          "content": "\t{\n   \t\"error\": \"404 - User's board is not found\"\n\t}",
          "type": "json"
        },
        {
          "title": "500",
          "content": "\t{\n   \t\"error\": \"500 - Can't get board information\"\n\t}",
          "type": "json"
        }
      ]
    },
    "filename": "./internal/app/handler/board.go",
    "groupTitle": "Board"
  },
  {
    "type": "get",
    "url": "api/v1/board",
    "title": "Get all user's boards",
    "name": "GetAllBoard",
    "group": "Board",
    "version": "1.0.0",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Bearer authorization token.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n    \"Authorization\": \"Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MjI2Njg0NzcsImlhdCI6MTYyMjYyNTI3NywidXNlcl9pZCI6MTB9.0aOR7SJHE9ycZa0ihPR6Q7rICCjKPauUbEiUJw16aPo\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "String[]",
            "optional": false,
            "field": "boards",
            "description": "<p>List of user's boards</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "200:",
          "content": "\t{\n  \t\"boards\": [\n        {\n            \"id\": 1,\n            \"title\": \"Study\",\n            \"description\": \"My study tasks\"\n        },\n        {\n            \"id\": 2,\n            \"title\": \"Work\",\n            \"description\": \"Board for work's tasks\"\n        }\n    ]\n\t}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "4xx": [
          {
            "group": "4xx",
            "type": "String",
            "optional": false,
            "field": "error",
            "description": "<p>Error description</p>"
          }
        ],
        "5xx": [
          {
            "group": "5xx",
            "type": "String",
            "optional": false,
            "field": "error",
            "description": "<p>Error description</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "400",
          "content": "\t{\n   \t\"error\": \"400 - JSON reading error invalid character '\\\\n' in string literal\"\n\t}",
          "type": "json"
        },
        {
          "title": "401",
          "content": "\t{\n   \t\"error\": \"401 - Error with getting user ID from token: token is expired by 11h59m2s\"\n\t}",
          "type": "json"
        },
        {
          "title": "500",
          "content": "\t{\n   \t\"error\": \"500 - dial tcp database.local:5432: connect: connection refused\"\n\t}",
          "type": "json"
        },
        {
          "title": "500",
          "content": "\t{\n   \t\"error\": \"500 - Can't get user ID from request\"\n\t}",
          "type": "json"
        }
      ]
    },
    "filename": "./internal/app/handler/board.go",
    "groupTitle": "Board"
  },
  {
    "type": "get",
    "url": "api/v1/board/:id",
    "title": "Get board by ID",
    "name": "GetAllBoard",
    "group": "Board",
    "version": "1.0.0",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Bearer authorization token.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n    \"Authorization\": \"Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MjI2Njg0NzcsImlhdCI6MTYyMjYyNTI3NywidXNlcl9pZCI6MTB9.0aOR7SJHE9ycZa0ihPR6Q7rICCjKPauUbEiUJw16aPo\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>User's board ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "String[]",
            "optional": false,
            "field": "boards",
            "description": "<p>List of user's boards</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "200:",
          "content": "\t{\n      \"id\": 1,\n      \"title\": \"Study\",\n      \"description\": \"My study tasks\"\n  }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "4xx": [
          {
            "group": "4xx",
            "type": "String",
            "optional": false,
            "field": "error",
            "description": "<p>Error description</p>"
          }
        ],
        "5xx": [
          {
            "group": "5xx",
            "type": "String",
            "optional": false,
            "field": "error",
            "description": "<p>Error description</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "400",
          "content": "\t{\n   \t\"error\": \"400 - Invalid ID param\"\n\t}",
          "type": "json"
        },
        {
          "title": "401",
          "content": "\t{\n   \t\"error\": \"401 - Invalid Authorization header\"\n\t}",
          "type": "json"
        },
        {
          "title": "404",
          "content": "\t{\n   \t\"error\": \"404 - User's board is not found\"\n\t}",
          "type": "json"
        },
        {
          "title": "500",
          "content": "\t{\n   \t\"error\": \"500 - Can't get board information\"\n\t}",
          "type": "json"
        }
      ]
    },
    "filename": "./internal/app/handler/board.go",
    "groupTitle": "Board"
  },
  {
    "type": "put",
    "url": "api/v1/board/:id",
    "title": "Update board data",
    "name": "UpdateBoard",
    "group": "Board",
    "version": "1.0.0",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Bearer authorization token.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n    \"Authorization\": \"Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MjI2Njg0NzcsImlhdCI6MTYyMjYyNTI3NywidXNlcl9pZCI6MTB9.0aOR7SJHE9ycZa0ihPR6Q7rICCjKPauUbEiUJw16aPo\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>User's board ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "boards",
            "description": "<p>List of user's boards</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "200:",
          "content": "\t{\n      \"updated_board_id\": 1\n  }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "4xx": [
          {
            "group": "4xx",
            "type": "String",
            "optional": false,
            "field": "error",
            "description": "<p>Error description</p>"
          }
        ],
        "5xx": [
          {
            "group": "5xx",
            "type": "String",
            "optional": false,
            "field": "error",
            "description": "<p>Error description</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "400",
          "content": "\t{\n   \t\"error\": \"400 - Invalid ID param\"\n\t}",
          "type": "json"
        },
        {
          "title": "401",
          "content": "\t{\n   \t\"error\": \"401 - Invalid Authorization header\"\n\t}",
          "type": "json"
        },
        {
          "title": "404",
          "content": "\t{\n   \t\"error\": \"404 - User's board is not found\"\n\t}",
          "type": "json"
        },
        {
          "title": "500",
          "content": "\t{\n   \t\"error\": \"500 - Can't get board information\"\n\t}",
          "type": "json"
        }
      ]
    },
    "filename": "./internal/app/handler/board.go",
    "groupTitle": "Board"
  },
  {
    "type": "get",
    "url": "/ping",
    "title": "Service healthcheck method",
    "name": "Ping",
    "group": "Healthcheck",
    "version": "1.0.0",
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "ping",
            "description": "<p>Success answer from service</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "200:",
          "content": "{\n  \"ping\": \"pong\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./internal/app/handler/ping.go",
    "groupTitle": "Healthcheck"
  }
] });
