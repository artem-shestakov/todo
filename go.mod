module gutlab.com/artem-shestakov/todo

go 1.16

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible // indirect
	github.com/go-playground/universal-translator v0.17.0 // indirect
	github.com/go-playground/validator v9.31.0+incompatible // indirect
	github.com/golang-migrate/migrate v3.5.4+incompatible // indirect
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/jmoiron/sqlx v1.3.4 // indirect
	github.com/joho/godotenv v1.3.0 // indirect
	github.com/leodido/go-urn v1.2.1 // indirect
	github.com/lib/pq v1.10.2 // indirect
	github.com/sirupsen/logrus v1.8.1 // indirect
	github.com/spf13/viper v1.7.1 // indirect
)
