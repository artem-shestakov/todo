package main

import (
	"os"

	"github.com/go-playground/validator"
	"github.com/joho/godotenv"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"gutlab.com/artem-shestakov/todo/internal/app/handler"
	"gutlab.com/artem-shestakov/todo/internal/app/repository"
	"gutlab.com/artem-shestakov/todo/internal/app/server"
	"gutlab.com/artem-shestakov/todo/internal/app/service"
)

var validate *validator.Validate

func main() {
	logrus.SetFormatter(new(logrus.JSONFormatter))
	if err := godotenv.Load(); err != nil {
		logrus.Fatalf("Eroor with laoading .env file: %s", err.Error())
	}

	db, err := repository.NewBD(&repository.Config{
		Address:  getEnv("DB_ADDRESS", "localhost"),
		Port:     getEnv("DB_PORT", "5432"),
		User:     getEnv("DB_USER", "postgres"),
		DBName:   getEnv("DB_NAME", "postgres"),
		Password: getEnv("DB_PASSWORD", "postgres"),
		SSLMode:  getEnv("DB_SSL_MODE", "disable"),
	})
	if err != nil {
		logrus.Fatalf("Error with database initialization: %s", err)
	}

	validate = validator.New()
	// validateStruct()
	// validateVariable()

	repo := repository.NewRepository(db)
	service := service.NewService(repo)
	handlers := handler.NewHandler(service, validate)

	// Create server config with server]
	srv := server.NewServer()

	// Start server
	if err := srv.Run("0.0.0.0", getEnv("PORT", "8000"), handlers.InitRoutes()); err != nil {
		logrus.Fatalln(err)
	}
}

func initConfig() error {
	viper.AddConfigPath("configs")
	viper.SetConfigName("configs")
	return viper.ReadInConfig()
}

// getEnv getting env variables or set default value
func getEnv(lookEnv string, defEnv string) string {
	env, exist := os.LookupEnv(lookEnv)
	if !exist {
		env = defEnv
	}
	return env
}
