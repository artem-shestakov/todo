package model

import (
	"encoding/json"
	"io"
)

type User struct {
	ID        int    `json:"id" db:"id"`
	FirstName string `json:"first_name" validate:"required"`
	LasteName string `json:"last_name" validate:"required"`
	Email     string `json:"email" validate:"required,email"`
	Password  string `json:"password" validate:"required"`
}

func (u *User) FromJSON(r io.Reader) error {
	decoder := json.NewDecoder(r)
	return decoder.Decode(u)
}

type SignInInput struct {
	Email    string `json:"email" validate:"required,email"`
	Password string `json:"password" validate:"required"`
}

func (s *SignInInput) FromJSON(r io.Reader) error {
	decoder := json.NewDecoder(r)
	return decoder.Decode(s)
}
