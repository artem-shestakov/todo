package model

import (
	"encoding/json"
	"errors"
	"io"
	"net/http"
)

type Board struct {
	ID          int    `json:"id" db:"id"`
	Title       string `json:"title" db:"title" validate:"required"`
	Description string `json:"description" db:"description"`
}

func (b *Board) FromJSON(r io.Reader) error {
	decoder := json.NewDecoder(r)
	return decoder.Decode(b)
}

type Boards struct {
	Boards []Board `json:"boards"`
}

func (b *Boards) ToJSON(rw http.ResponseWriter) error {
	rw.Header().Set("Content-Type", "application/json")
	encoder := json.NewEncoder(rw)
	return encoder.Encode(b)
}

type UpdateBoard struct {
	Title       *string `json:"title"`
	Description *string `json:"description"`
}

func (b *UpdateBoard) FromJSON(r io.Reader) error {
	decoder := json.NewDecoder(r)
	return decoder.Decode(b)
}

func (b *UpdateBoard) Validate() error {
	if b.Title == nil && b.Description == nil {
		return errors.New("Update values are empty")
	}
	return nil
}

type UsersBoard struct {
	ID      int
	UserID  int
	BoardID int
}
