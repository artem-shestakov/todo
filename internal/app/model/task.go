package model

type Task struct {
	ID          int    `json:"id"`
	Title       string `json:"title"`
	Description string `json:"description"`
	Done        bool   `json:"done"`
}

// type ListTask struct {
// 	ID     int
// 	TaskID int
// 	ListID int
// }
