package handler

import (
	"net/http"
	"strconv"

	"gutlab.com/artem-shestakov/todo/internal/app/model"
)

/**
* @api {post} /auth/sign-in Get JWT token
* @apiGroup Auth
* @apiName SignIn
*
* @apiversion 1.0.0
*
* @apiParam {String} email User's email (login)
* @apiParam {String} password User login password
*
* @apiParamExample {json} Example:
*	{
*    	"email": "artem.s.shestakov@yandex.ru",
*   	"password": "userPassword"
*	}
*
* @apiSuccess (200) {String} token JWT token
*
* @apiSuccessExample {json} 200:
*	{
*   	"token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MjI1NzI4MTQsImlhdCI6MTYyMjUyOTYxNCwidXNlcl9pZCI6Nn0.8Pg3iFwsC1hs_y3VHPsllgh_szeZkxVUNZ4a5YunTy4"
*	}
*
* @apiError (4xx) {String} error Error description
*
* @apiErrorExample {json} 400:
*	{
*    	"error": "400 - Unmarshall error: invalid character '\\n' in string literal"
*	}
*
* @apiError (5xx) {String} error Error description
*
* @apiErrorExample {json} 500:
*	{
*    	"error": "500 - Sign in error: sql: no rows in result set"
*	}
 */
func (h *Handler) SignIn() http.HandlerFunc {
	input := model.SignInInput{}
	return func(rw http.ResponseWriter, r *http.Request) {
		if err := input.FromJSON(r.Body); err != nil {
			h.errorResponse(rw, http.StatusBadRequest, "Unmarshall error: %s"+err.Error())
			return
		}
		if err := h.validator.Struct(input); err != nil {
			h.errorResponse(rw, http.StatusBadRequest, "JSON fields error: "+err.Error())
			return
		}
		token, err := h.service.Authorization.GenerateToken(input.Email, input.Password)
		if err != nil {
			h.errorResponse(rw, http.StatusInternalServerError, "Sign in error: "+err.Error())
			return
		}
		h.Response(rw, http.StatusOK, map[string]interface{}{
			"token": token,
		})
	}
}

/**
* @api {post} /auth/sigh-up Create new user
* @apiName SignUp
* @apiGroup Auth
* @apiversion 1.0.0
*
* @apiParam {String} first_name User's first name
* @apiParam {String} last_name User's last name
* @apiParam {String} email User's email (login)
* @apiParam {String} password User login password
*
* @apiParamExample {json} Example:
*	{
*    	"first_name": "Artem",
*    	"last_name": "Shestakov",
*    	"email": "artem.s.shestakov@yandex.ru",
*   	"password": "userPassword"
*	}
*
* @apiSuccess (201) {String} user_id Created user's ID
*
* @apiSuccessExample {json} 201:
*	{
*   	"user_id": "1"
*	}
*
* @apiError (4xx) {String} error Error description
*
* @apiErrorExample {json} 400
*	{
*    	"error": "400 - Error with unmarshall JSON: invalid character '\\n' in string literal"
*	}
*
* @apiError (5xx) {String} error Error description
*
* @apiErrorExample {json} 500
*	{
*    	"error": "500 - Create user error: pq: duplicate key value violates unique constraint \"users_email_key\""
*	}
 */
func (h *Handler) SignUp() http.HandlerFunc {
	user := model.User{}
	return func(rw http.ResponseWriter, r *http.Request) {
		if err := user.FromJSON(r.Body); err != nil {
			h.errorResponse(rw, http.StatusBadRequest, "Unmarshall error: "+err.Error())
			return
		}
		if err := h.validator.Struct(user); err != nil {
			h.errorResponse(rw, http.StatusBadRequest, "JSON fields error: "+err.Error())
			return
		}

		id, err := h.service.Authorization.CreateUser(&user)
		if err != nil {
			h.errorResponse(rw, http.StatusInternalServerError, "Create user error: "+err.Error())
			return
		}
		h.Response(rw, http.StatusCreated, map[string]interface{}{
			"user_id": strconv.Itoa(id),
		})
	}
}
