package handler

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/go-playground/validator"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"gutlab.com/artem-shestakov/todo/internal/app/service"
)

type Answer struct {
	Code    int
	Message string `json:"message"`
}

type Handler struct {
	service   *service.Service
	validator *validator.Validate
}

func NewHandler(service *service.Service, validator *validator.Validate) *Handler {
	return &Handler{
		service:   service,
		validator: validator,
	}
}

func (h *Handler) Response(rw http.ResponseWriter, code int, message interface{}) {
	rw.Header().Set("Content-Type", "application/json")
	rw.WriteHeader(code)
	jsonBytes, err := json.Marshal(message)
	if err != nil {
		json.NewEncoder(rw).Encode(map[string]string{
			"error": "500 - Marshall to JSON error " + err.Error(),
		})
		return
	}
	rw.Write(jsonBytes)
}

func (h *Handler) errorResponse(w http.ResponseWriter, code int, message string) {
	logrus.Error(message)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	json.NewEncoder(w).Encode(map[string]string{
		"error": fmt.Sprintf("%d - %s", code, message),
	})
}

func (h *Handler) InitRoutes() *mux.Router {
	router := mux.NewRouter()

	// Healthcheck endpoint
	router.Handle("/ping", h.Ping()).Methods("GET")

	// Auth endpoints
	authRoute := router.PathPrefix("/auth").Subrouter()
	authRoute.Handle("/sign-in", h.SignIn()).Methods("POST")
	authRoute.Handle("/sign-up", h.SignUp()).Methods("POST")

	// API endpoints v1
	apiV1Route := router.PathPrefix("/api/v1").Subrouter()
	apiV1Route.Use(h.AuthMiddleware)

	boardV1Route := apiV1Route.PathPrefix("/boards").Subrouter()
	boardV1Route.Handle("", h.CreateBoard()).Methods("POST")
	boardV1Route.Handle("", h.GetAllBoard()).Methods("GET")
	boardV1Route.Handle("/{id:[0-9]+}", h.GetBoardByID()).Methods("GET")
	boardV1Route.Handle("/{id:[0-9]+}", h.UpdateBoard()).Methods("PUT")
	boardV1Route.Handle("/{id:[0-9]+}", h.DeleteBoard()).Methods("DELETE")

	return router
}
