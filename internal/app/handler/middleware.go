package handler

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"strings"

	"github.com/sirupsen/logrus"
)

func (h *Handler) AuthMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
		authHeader := r.Header.Get("Authorization")
		if authHeader == "" {
			h.errorResponse(rw, http.StatusUnauthorized, "Empty Authorization header")
			return
		}

		bearer := strings.Split(authHeader, " ")
		if len(bearer) != 2 {
			h.errorResponse(rw, http.StatusUnauthorized, "Invalid Authorization header")
			return
		}

		userID, err := h.service.ParseToken(bearer[1])
		if err != nil {
			h.errorResponse(rw, http.StatusUnauthorized, fmt.Sprintf("Error with getting user ID from token: %s", err))
			return
		}
		logrus.Infof("User ID %d send request %s %s", userID, r.RequestURI, r.Method)
		ctx := context.WithValue(r.Context(), "userID", userID)
		next.ServeHTTP(rw, r.WithContext(ctx))
	})
}

func (h *Handler) GetUserID(rw http.ResponseWriter, r *http.Request) (int, error) {
	user_id := r.Context().Value("userID")
	if user_id == "" {
		h.errorResponse(rw, http.StatusForbidden, "User ID is not found in request")
		return 0, errors.New("User ID is not found in request")
	}
	id, ok := user_id.(int)
	if !ok {
		h.errorResponse(rw, http.StatusForbidden, "User ID is of invalid type")
		return 0, errors.New("User ID is of invalid type")
	}
	return id, nil
}
