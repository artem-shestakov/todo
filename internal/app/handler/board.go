package handler

import (
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"gutlab.com/artem-shestakov/todo/internal/app/model"
)

/**
* @api {post} api/v1/board Create new board
* @apiName CreateBoard
* @apiGroup Board
* @apiversion 1.0.0
*
* @apiHeader {String} Authorization Bearer authorization token.
*
* @apiHeaderExample {json} Header-Example:
*   {
*       "Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MjI2Njg0NzcsImlhdCI6MTYyMjYyNTI3NywidXNlcl9pZCI6MTB9.0aOR7SJHE9ycZa0ihPR6Q7rICCjKPauUbEiUJw16aPo"
*   }
*
* @apiParam {String} titte Board title
* @apiParam {String} [description] Board description
*
* @apiParamExample {json} Example:
*	{
*    	"title": "Work",
*    	"description": "Board for work tasks",
*	}
*
* @apiSuccess (201) {String} user_id Created board's ID
*
* @apiSuccessExample {json} 201:
*	{
*   	"board_id": "1"
*	}
*
* @apiError (4xx) {String} error Error description
*
* @apiErrorExample {json} 400
*	{
*    	"error": "400 - JSON reading error invalid character '\\n' in string literal"
*	}
*
* @apiErrorExample {json} 401
*	{
*    	"error": "401 - Empty Authorization header"
*	}
*
* @apiError (5xx) {String} error Error description
*
* @apiErrorExample {json} 500
*	{
*    	"error": "500 - dial tcp database.local:5432: connect: connection refused"
*	}
* @apiErrorExample {json} 500
*	{
*    	"error": "500 - Can't get user ID from request"
*	}
 */
func (h *Handler) CreateBoard() http.HandlerFunc {
	board := model.Board{}
	return func(rw http.ResponseWriter, r *http.Request) {
		user_id, err := h.GetUserID(rw, r)
		if err != nil {
			h.errorResponse(rw, http.StatusInternalServerError, "Can't get user ID from request")
			return
		}
		if err := board.FromJSON(r.Body); err != nil {
			h.errorResponse(rw, http.StatusBadRequest, "JSON reading error "+err.Error())
			return
		}
		board_id, err := h.service.Board.Create(user_id, board)
		if err != nil {
			h.errorResponse(rw, http.StatusInternalServerError, err.Error())
			return
		}

		h.Response(rw, http.StatusCreated, map[string]interface{}{
			"board_id": board_id,
		})
	}
}

/**
* @api {get} api/v1/board Get all user's boards
* @apiName GetAllBoard
* @apiGroup Board
* @apiversion 1.0.0
*
* @apiHeader {String} Authorization Bearer authorization token.
*
* @apiHeaderExample {json} Header-Example:
*   {
*       "Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MjI2Njg0NzcsImlhdCI6MTYyMjYyNTI3NywidXNlcl9pZCI6MTB9.0aOR7SJHE9ycZa0ihPR6Q7rICCjKPauUbEiUJw16aPo"
*   }
*
* @apiSuccess (200) {String[]} boards List of user's boards
*
* @apiSuccessExample {json} 200:
*	{
*   	"boards": [
        {
            "id": 1,
            "title": "Study",
            "description": "My study tasks"
        },
        {
            "id": 2,
            "title": "Work",
            "description": "Board for work's tasks"
        }
    ]
*	}
*
* @apiError (4xx) {String} error Error description
*
* @apiErrorExample {json} 400
*	{
*    	"error": "400 - JSON reading error invalid character '\\n' in string literal"
*	}
*
* @apiErrorExample {json} 401
*	{
*    	"error": "401 - Error with getting user ID from token: token is expired by 11h59m2s"
*	}
*
* @apiError (5xx) {String} error Error description
*
* @apiErrorExample {json} 500
*	{
*    	"error": "500 - dial tcp database.local:5432: connect: connection refused"
*	}
* @apiErrorExample {json} 500
*	{
*    	"error": "500 - Can't get user ID from request"
*	}
*/
func (h *Handler) GetAllBoard() http.HandlerFunc {
	userBoards := new(model.Boards)
	return func(rw http.ResponseWriter, r *http.Request) {
		user_id, err := h.GetUserID(rw, r)
		if err != nil {
			h.errorResponse(rw, http.StatusInternalServerError, "Can't get user ID from request")
			return
		}

		boards, err := h.service.Board.GetAll(user_id)
		if err != nil {
			h.errorResponse(rw, http.StatusInternalServerError, "Can't get user's boards "+err.Error())
		}

		userBoards.Boards = boards
		h.Response(rw, http.StatusOK, userBoards)
	}
}

/**
* @api {get} api/v1/board/:id Get board by ID
* @apiName GetAllBoard
* @apiGroup Board
* @apiversion 1.0.0
*
* @apiHeader {String} Authorization Bearer authorization token.
*
* @apiHeaderExample {json} Header-Example:
*   {
*       "Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MjI2Njg0NzcsImlhdCI6MTYyMjYyNTI3NywidXNlcl9pZCI6MTB9.0aOR7SJHE9ycZa0ihPR6Q7rICCjKPauUbEiUJw16aPo"
*   }
*
* @apiParam {Number} id User's board ID.
*
* @apiSuccess (200) {String[]} boards List of user's boards
*
* @apiSuccessExample {json} 200:
*	{
*       "id": 1,
*       "title": "Study",
*       "description": "My study tasks"
*   }
*
* @apiError (4xx) {String} error Error description
*
* @apiErrorExample {json} 400
*	{
*    	"error": "400 - Invalid ID param"
*	}
*
* @apiErrorExample {json} 401
*	{
*    	"error": "401 - Invalid Authorization header"
*	}
*
* @apiErrorExample {json} 404
*	{
*    	"error": "404 - User's board is not found"
*	}
*
* @apiError (5xx) {String} error Error description
*
* @apiErrorExample {json} 500
*	{
*    	"error": "500 - Can't get board information"
*	}
 */
func (h *Handler) GetBoardByID() http.HandlerFunc {
	return func(rw http.ResponseWriter, r *http.Request) {
		user_id, err := h.GetUserID(rw, r)
		if err != nil {
			h.errorResponse(rw, http.StatusInternalServerError, "Can't get user ID from request")
			return
		}

		vars := mux.Vars(r)
		board_id, err := strconv.Atoi(vars["id"])
		if err != nil {
			h.errorResponse(rw, http.StatusBadRequest, "Invalid ID param")
			return
		}

		board, err := h.service.Board.GetByID(user_id, board_id)
		if err != nil {
			if err.Error() == "sql: no rows in result set" {
				h.errorResponse(rw, http.StatusNotFound, "User's board is not found")
				return
			}
			h.errorResponse(rw, http.StatusInternalServerError, "Can't get board information "+err.Error())
			return
		}
		h.Response(rw, http.StatusOK, board)
	}
}

/**
* @api {put} api/v1/board/:id Update board data
* @apiName UpdateBoard
* @apiGroup Board
* @apiversion 1.0.0
*
* @apiHeader {String} Authorization Bearer authorization token.
*
* @apiHeaderExample {json} Header-Example:
*   {
*       "Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MjI2Njg0NzcsImlhdCI6MTYyMjYyNTI3NywidXNlcl9pZCI6MTB9.0aOR7SJHE9ycZa0ihPR6Q7rICCjKPauUbEiUJw16aPo"
*   }
*
* @apiParam {Number} id User's board ID.
*
* @apiSuccess (200) {String} boards List of user's boards
*
* @apiSuccessExample {json} 200:
*	{
*       "updated_board_id": 1
*   }
*
* @apiError (4xx) {String} error Error description
*
* @apiErrorExample {json} 400
*	{
*    	"error": "400 - Invalid ID param"
*	}
*
* @apiErrorExample {json} 401
*	{
*    	"error": "401 - Invalid Authorization header"
*	}
*
* @apiErrorExample {json} 404
*	{
*    	"error": "404 - User's board is not found"
*	}
*
* @apiError (5xx) {String} error Error description
*
* @apiErrorExample {json} 500
*	{
*    	"error": "500 - Can't get board information"
*	}
 */
func (h *Handler) UpdateBoard() http.HandlerFunc {
	updateBoard := new(model.UpdateBoard)
	return func(rw http.ResponseWriter, r *http.Request) {
		user_id, err := h.GetUserID(rw, r)
		if err != nil {
			h.errorResponse(rw, http.StatusInternalServerError, "Can't get user ID from request")
			return
		}

		vars := mux.Vars(r)
		board_id, err := strconv.Atoi(vars["id"])
		if err != nil {
			h.errorResponse(rw, http.StatusBadRequest, "Invalid ID param")
			return
		}

		if err := updateBoard.FromJSON(r.Body); err != nil {
			h.errorResponse(rw, http.StatusBadRequest, "JSON reading error "+err.Error())
			return
		}

		if err := h.service.Board.Update(user_id, board_id, updateBoard); err != nil {
			if err.Error() == "sql: no rows in result set" {
				h.errorResponse(rw, http.StatusNotFound, "User has no this board")
				return
			}
			h.errorResponse(rw, http.StatusBadRequest, err.Error())
			return
		}

		h.Response(rw, http.StatusOK, map[string]interface{}{
			"updated_board_id": board_id,
		})
	}
}

/**
* @api {delete} api/v1/board/:id Delete user's board
* @apiName DeleteBoard
* @apiGroup Board
* @apiversion 1.0.0
*
* @apiHeader {String} Authorization Bearer authorization token.
*
* @apiHeaderExample {json} Header-Example:
*   {
*       "Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MjI2Njg0NzcsImlhdCI6MTYyMjYyNTI3NywidXNlcl9pZCI6MTB9.0aOR7SJHE9ycZa0ihPR6Q7rICCjKPauUbEiUJw16aPo"
*   }
*
* @apiParam {Number} id User's board ID.
*
* @apiSuccess (200) {String} boards List of user's boards
*
* @apiSuccessExample {json} 200:
*	{
*       "deleted_board_id": 1
*   }
*
* @apiError (4xx) {String} error Error description
*
* @apiErrorExample {json} 400
*	{
*    	"error": "400 - Invalid ID param"
*	}
*
* @apiErrorExample {json} 401
*	{
*    	"error": "401 - Invalid Authorization header"
*	}
*
* @apiErrorExample {json} 404
*	{
*    	"error": "404 - User's board is not found"
*	}
*
* @apiError (5xx) {String} error Error description
*
* @apiErrorExample {json} 500
*	{
*    	"error": "500 - Can't get board information"
*	}
 */
func (h *Handler) DeleteBoard() http.HandlerFunc {
	return func(rw http.ResponseWriter, r *http.Request) {
		user_id, err := h.GetUserID(rw, r)
		if err != nil {
			h.errorResponse(rw, http.StatusInternalServerError, "Can't get user ID from request")
			return
		}

		vars := mux.Vars(r)
		board_id, err := strconv.Atoi(vars["id"])
		if err != nil {
			h.errorResponse(rw, http.StatusBadRequest, "Invalid ID param")
		}

		err = h.service.Board.Delete(user_id, board_id)
		if err != nil {
			if err.Error() == "sql: no rows in result set" {
				h.errorResponse(rw, http.StatusNotFound, "User has no this board")
				return
			}
			h.errorResponse(rw, http.StatusInternalServerError, "Can't delete board "+err.Error())
			return
		}
		h.Response(rw, http.StatusOK, map[string]interface{}{
			"deleted_board_id": board_id,
		})
	}
}
