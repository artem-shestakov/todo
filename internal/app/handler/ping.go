package handler

import (
	"net/http"
)

/**
 * @api {get} /ping Service healthcheck method
 * @apiName Ping
 * @apiGroup Healthcheck
 *
 * @apiversion 1.0.0
 *
 * @apiSuccess (200) {String} ping Success answer from service
 *
 * @apiSuccessExample 200:
 *     {
 *       "ping": "pong"
 *     }
 */
func (h *Handler) Ping() http.HandlerFunc {
	return func(rw http.ResponseWriter, r *http.Request) {
		h.Response(rw, http.StatusOK, map[string]interface{}{
			"ping": "pong",
		})
	}
}
