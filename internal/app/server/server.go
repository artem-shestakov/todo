package server

import (
	"context"
	"net/http"

	"github.com/sirupsen/logrus"
)

type Server struct {
	httpServer *http.Server
	logger     *logrus.Logger
}

func NewServer() *Server {
	return &Server{}
}

func (s *Server) Run(address string, port string, handler http.Handler) error {
	// s.setLogLevel(logLevel)
	// s.logger.SetFormatter(new(logrus.JSONFormatter))

	s.httpServer = &http.Server{
		Addr:    address + ":" + port,
		Handler: handler,
	}
	logrus.Infof("Starting server on http://%s", s.httpServer.Addr)
	return s.httpServer.ListenAndServe()
}

func (s *Server) Shutdown(ctx context.Context) error {
	return s.httpServer.Shutdown(ctx)
}

// func (s *Server) setLogLevel(level string) {
// 	switch level {
// 	case "debug":
// 		s.logger.SetLevel(logrus.DebugLevel)
// 	case "warn":
// 		s.logger.SetLevel(logrus.WarnLevel)
// 	default:
// 		s.logger.SetLevel(logrus.InfoLevel)
// 	}
// }
