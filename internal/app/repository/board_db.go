package repository

import (
	"fmt"
	"strings"

	"github.com/jmoiron/sqlx"
	"github.com/sirupsen/logrus"
	"gutlab.com/artem-shestakov/todo/internal/app/model"
)

type BoardDB struct {
	db *sqlx.DB
}

func NewBoardDB(db *sqlx.DB) *BoardDB {
	return &BoardDB{
		db: db,
	}
}

func (r *BoardDB) Create(user_id int, board model.Board) (int, error) {
	tx, err := r.db.Begin()
	if err != nil {
		return 0, err
	}
	var board_id int
	createBoardQuery := fmt.Sprintf("INSERT INTO %s (title, description) VALUES ($1, $2) RETURNING id", boardTable)
	row := tx.QueryRow(createBoardQuery, board.Title, board.Description)
	if err := row.Scan(&board_id); err != nil {
		tx.Rollback()
		return 0, err
	}

	createUsersBoardQuery := fmt.Sprintf("INSERT INTO %s (user_id, board_id) VALUES ($1, $2)", usersBoards)
	_, err = tx.Exec(createUsersBoardQuery, user_id, board_id)
	if err != nil {
		tx.Rollback()
		return 0, err
	}
	return board_id, tx.Commit()
}

func (r *BoardDB) GetAll(user_id int) ([]model.Board, error) {
	var boards []model.Board

	query := fmt.Sprintf("SELECT b.id, b.title, b.description FROM %s b INNER JOIN %s ub ON b.id=ub.board_id WHERE ub.user_id = $1", boardTable, usersBoards)
	err := r.db.Select(&boards, query, user_id)
	return boards, err
}

func (r *BoardDB) GetByID(user_id, board_id int) (model.Board, error) {
	var board model.Board

	query := fmt.Sprintf("SELECT b.id, b.title, b.description FROM %s b INNER JOIN %s ub ON b.id=ub.board_id WHERE ub.user_id = $1 AND ub.board_id = $2", boardTable, usersBoards)
	err := r.db.Get(&board, query, user_id, board_id)
	return board, err
}

func (r *BoardDB) Delete(user_id, board_id int) error {
	_, err := r.GetByID(user_id, board_id)
	if err != nil {
		return err
	}
	query := fmt.Sprintf("DELETE FROM %s b USING %s ub WHERE b.id = ub.board_id AND ub.user_id = $1 AND ub.board_id = $2", boardTable, usersBoards)
	_, err = r.db.Exec(query, user_id, board_id)
	return err
}

func (r *BoardDB) Update(user_id, board_id int, updateBoard *model.UpdateBoard) error {
	_, err := r.GetByID(user_id, board_id)
	if err != nil {
		return err
	}

	setValues := make([]string, 0)
	args := make([]interface{}, 0)
	argId := 1

	if updateBoard.Title != nil {
		setValues = append(setValues, fmt.Sprintf("title=$%d", argId))
		args = append(args, updateBoard.Title)
		argId++
	}
	if updateBoard.Description != nil {
		setValues = append(setValues, fmt.Sprintf("description=$%d", argId))
		args = append(args, updateBoard.Description)
		argId++
	}
	args = append(args, board_id, user_id)

	setQuery := strings.Join(setValues, ", ")
	query := fmt.Sprintf("UPDATE %s b SET %s FROM %s ub WHERE b.id = ub.board_id AND ub.board_id = $%d AND ub.user_id = $%d", boardTable, setQuery, usersBoards, argId, argId+1)

	logrus.Debug("UpdateQuery:", query)
	logrus.Debug("args:", args)

	_, err = r.db.Exec(query, args...)
	return err
}
