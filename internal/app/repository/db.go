package repository

import (
	"fmt"
	"log"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

const (
	usersTable  = "users"
	boardTable  = "boards"
	usersBoards = "users_boards"
	listTable   = "lists"
	taskTable   = "tasks"
)

type Config struct {
	Address  string
	Port     string
	User     string
	Password string
	DBName   string
	SSLMode  string
}

func NewBD(config *Config) (*sqlx.DB, error) {
	db, err := sqlx.Open("postgres", fmt.Sprintf("host=%s port=%s user=%s dbname=%s password=%s sslmode=%s",
		config.Address, config.Port, config.User, config.DBName, config.Password, config.SSLMode))
	if err != nil {
		return nil, err
	}
	err = db.Ping()
	if err != nil {
		log.Fatalf("Can't ping database: %s", err)
		return nil, err
	}
	return db, nil
}
