package repository

import (
	"github.com/jmoiron/sqlx"
	"gutlab.com/artem-shestakov/todo/internal/app/model"
)

type Authorization interface {
	CreateUser(user *model.User) (int, error)
	GetUser(email, password string) (model.User, error)
}

type Board interface {
	Create(user_id int, board model.Board) (int, error)
	GetAll(user_id int) ([]model.Board, error)
	GetByID(user_id, board_id int) (model.Board, error)
	Delete(user_id, board_id int) error
	Update(user_id, board_id int, updateBoard *model.UpdateBoard) error
}

type ToDoList interface {
}

type Repository struct {
	Authorization
	Board
	ToDoList
}

func NewRepository(db *sqlx.DB) *Repository {
	return &Repository{
		Authorization: NewAuthDB(db),
		Board:         NewBoardDB(db),
	}
}
