package service

import (
	"gutlab.com/artem-shestakov/todo/internal/app/model"
	"gutlab.com/artem-shestakov/todo/internal/app/repository"
)

type BoardService struct {
	repository repository.Board
}

func NewBoardService(repository repository.Board) *BoardService {
	return &BoardService{
		repository: repository,
	}
}

func (s *BoardService) Create(user_id int, board model.Board) (int, error) {
	return s.repository.Create(user_id, board)
}

func (s *BoardService) GetAll(user_id int) ([]model.Board, error) {
	return s.repository.GetAll(user_id)
}

func (s *BoardService) GetByID(user_id, board_id int) (model.Board, error) {
	return s.repository.GetByID(user_id, board_id)
}

func (s *BoardService) Delete(user_id, board_id int) error {
	return s.repository.Delete(user_id, board_id)
}

func (s *BoardService) Update(user_id, board_id int, updateBoard *model.UpdateBoard) error {
	if err := updateBoard.Validate(); err != nil {
		return err
	}
	return s.repository.Update(user_id, board_id, updateBoard)
}
