package service

import (
	"gutlab.com/artem-shestakov/todo/internal/app/model"
	"gutlab.com/artem-shestakov/todo/internal/app/repository"
)

type Authorization interface {
	CreateUser(user *model.User) (int, error)
	GenerateToken(email, password string) (string, error)
	ParseToken(token string) (int, error)
}

type Board interface {
	Create(user_id int, board model.Board) (int, error)
	GetAll(user_id int) ([]model.Board, error)
	GetByID(user_id, board_id int) (model.Board, error)
	Delete(user_id, board_id int) error
	Update(user_id, board_id int, updateBoard *model.UpdateBoard) error
}

type List interface {
}

type Service struct {
	Authorization
	Board
	List
}

func NewService(repo *repository.Repository) *Service {
	return &Service{
		Authorization: NewAuthService(repo.Authorization),
		Board:         NewBoardService(repo.Board),
	}
}
